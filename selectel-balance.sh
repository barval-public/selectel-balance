#!/bin/bash
set -eu

export PATH="/usr/local/bin:/usr/bin:/bin"
MUTT=$(which mutt)
JQ=$(which jq)
SCRIPT=$(readlink -e $0)
SCRIPT_PATH="$(dirname ${SCRIPT})"
#echo "SCRIPT_PATH=${SCRIPT_PATH}"
cd ${SCRIPT_PATH}

# Проверка наличия почтового клиента mutt
if [[ -z "${MUTT}" ]]; then
    echo "Ошибка - почтовый клиент mutt не найден"
    exit 1
fi

# Проверка наличия утилиты jq
if [[ -z "${JQ}" ]]; then
    echo "Ошибка - утилита jq не найдена"
    exit 1
fi

# Проверка наличия файла с переменными .env
if  [[ ! -f ".env" ]]; then
    echo "Ошибка - отсутствует файл с переменными .env"
    exit 2
fi

# Входные переменные берём из .env
source .env

# Проверка заполнения переменной SELECTEL_API_KEY
if  [[ -z "${SELECTEL_API_KEY}" ]]; then
    echo "Ошибка - пустая переменная SELECTEL_API_KEY"
    exit 3
#else
#    echo "SELECTEL_API_KEY=${SELECTEL_API_KEY}"
fi

# Проверка заполнения переменной MIN_BALANCE
if  [[ -z "${MIN_BALANCE}" ]]; then
    echo "Ошибка - пустая переменная MIN_BALANCE"
    exit 3
#else
#    echo "MIN_BALANCE=${MIN_BALANCE}"
fi

# Проверка заполнения переменной EMAIL_FOR_NOTIFICATION
if  [[ -z "${EMAIL_FOR_NOTIFICATION}" ]]; then
    echo "Ошибка - пустая переменная EMAIL_FOR_NOTIFICATION"
    exit 3
#else
#    echo "EMAIL_FOR_NOTIFICATION=${EMAIL_FOR_NOTIFICATION}"
fi

# Получаем баланс
ANSWER=$(curl -s -H "X-token: ${SELECTEL_API_KEY}" -H "Content-Type: application/json" https://my.selectel.ru/api/v3/billing/balance)
#echo "ANSWER=${ANSWER}"

# Проверяем прошла ли авторизация
if  [[ ! -z $(echo ${ANSWER} | grep "401 Authorization Required") ]]; then
    echo "Ошибка - не пройдена авторизация на SELECTEL:/n${ANSWER}"
    exit 4
else
    BALANCE=$(echo ${ANSWER} | jq '.data.primary.main')
#    echo "BALANCE=${BALANCE}"
fi

# Проверка заполнения переменной BALANCE
if  [[ -z "${BALANCE}" ]]; then
    echo "Ошибка - не получен BALANCE"
    exit 3
#else
#    echo "BALANCE=${BALANCE}"
fi

# Подготавливаем переменные BALANCE_RUB и MIN_BALANCE_RUB
BALANCE_RUB=$(echo ${BALANCE} | rev); BALANCE_RUB=${BALANCE_RUB:0:2}.${BALANCE_RUB:2}; BALANCE_RUB=$(echo $BALANCE_RUB | rev)
MIN_BALANCE_RUB=$(echo ${MIN_BALANCE} | rev); MIN_BALANCE_RUB=${MIN_BALANCE_RUB:0:2}.${MIN_BALANCE_RUB:2}; MIN_BALANCE_RUB=$(echo $MIN_BALANCE_RUB | rev)

# Сравниваем BALANCE с MIN_BALANCE
if [[ "${BALANCE}" -le "${MIN_BALANCE}" ]]; then
    echo "Баланс на Selectel нужно пополнить (${BALANCE_RUB} руб. < ${MIN_BALANCE_RUB} руб.)"
    # Отправляем уведомление на e-mail
    echo "Баланс на Selectel нужно пополнить (${BALANCE_RUB} руб. < ${MIN_BALANCE_RUB} руб.)" | mutt -s "Баланс на Selectel нужно пополнить (< ${MIN_BALANCE_RUB} руб.)" $EMAIL_FOR_NOTIFICATION
else
    echo "Баланс на Selectel достаточен (${BALANCE_RUB} руб. > ${MIN_BALANCE_RUB} руб.)"
fi

# Выход
exit 0
